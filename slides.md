<section data-background-color="#F2F3EE">

# Formació FLOSS

![logo-pap](img/pamapam-logo-bn.png "Pam A Pam")

Àngel Fernández - Anna Julià Verdaguer - Eric Marín
<!-- .element: class="text-clear-bk" -->

---

<section data-background-color="#2A2A2A">

## Què ens ve al cap quan pensem en FLOSS?

---

<section data-background-color="#F2F3EE">

![floss](img/floss.jpg "Foto del Toni Cantero, gràcies!")

<!-- .element: class="fragment fade-in-then-semi-out" -->

_Envieu-nos per missatge privat **3 conceptes** que us vinguin al cap!_
<!-- .element: class="fragment fade-up" -->

Note:
Sigueu pacients per recopilar les paraules i generar el núvol de paraules!
Compartir pantalla amb el núvol de paraules generat. Comentar quins conceptes són els
que més surten i veure si els podem anar atacant al llarg de la formació.
(Anna)

---

<section data-background-color="#2A2A2A">

## Què és el programari lliure?

Note:
Aquest capítol és del 2 (Àngel)

---
<section data-background-color="#F2F3EE">

### Breu història del FLOSS

- Als 60-70, el software era gratis amb el hardware
- Als 80, Unix posa restriccions
- 1984, Stallman crea el software lliure amb GNU
- 1991, Linus Torvalds afegeix Linux al projecte

Note:
Els programadors de l'epoca dels 70 compartien lliurement el software
Popularització, context entrada computadores multiusuari, a casa
Historia Stallman impressora, GNU is Not Unix
Linux es el Kernel, GNU tenia components de l'envoltori

---
<section data-background-color="#F2F3EE">

### Llibertats del programari lliure

_La llibertat de..._

0. Fer servir el programa, amb qualsevol propòsit.
1. Estudiar com funciona el programa i modificar-lo, adaptant-lo a les teves necessitats.
2. Distribuir còpies del programa.
3. Millorar el programa i fer públiques aquestes millores per a tothom, de manera que tota la comunitat en surti beneficiada.

Note:
Les llibertats 1 i 3 requereixen accés al codi font perquè estudiar
i modificar el programari sense el seu codi font és molt poc viable.

---

<!-- .slide: data-background="img/tipus_llicencies.png" -->

Note:
No mireu la taula! Simplement és per il·lustrar, amb els tipus n'hi ha prou
Explicar també llicencies com No Harm

---
<section data-background-color="#F2F3EE">

## Com encaixa el software lliure amb l'ESS?

 _No deixem que ens xuclin al capitalisme_
 <!-- .element: class="fragment fade-up" -->

 _Fem de la gestió comunitaria la resposta_
<!-- .element: class="fragment fade-down" -->

---
![share](img/3-agriculture.png "FSF")

---

<section data-background-color="#F2F3EE">

## I quan estic fent el qüestionari, què?

---
<section data-background-color="#F2F3EE">

- _Estem pendents de fer-ho_

**És per avui!    🐢**
<!-- .element: class="text-alt fragment fade-up" -->

- _Treballo desde casa_

**I el teu ordinador personal? Com et dones a coneixer?**
<!-- .element: class="text-alt fragment fade-up" -->

- _Nosaltres no generem cap contingut_

**No solament parlem de contingut digital**
<!-- .element: class="text-alt fragment fade-up" -->

Note:
Ahir millor que demà
Cuiner, fusteria.
Just because they are posted in a public domain where sharing is encouraged, does not mean they are not protected by copyright
https://www.buzzfeednews.com/article/jwherrman/want-to-publish-a-twitter-image-legally-just-embe
---

<section data-background-color="#F2F3EE">

### Nosaltres ho posem tot a la web!
 ![](img/ll-pap.png "Llicencia Pam a Pam")
 <!-- .element: class="fragment fade-up" -->

 _Copyleft_
 <!-- .element: class="fragment fade-up" -->
---
 ![](img/ll-pap2.png "Llicencia Pam a Pam")

---
<section data-background-color="#F2F3EE">

### Fem servir software gratis 🤷

**Gratis != Lliure**

<!-- .element: class="fragment fade-in" -->

> Si no pagues pel producte... llavors el producte ets tu!

<!-- .element: class="fragment fade-in" -->

Note:
Diapo del 2 (Àngel)
Hi ha software lliure de pagament. Pot ser millor pagar per software privatiu
que regalar dades a Google.
Exemple discoteques dones gratis
Explicar negoci de les dades, capitalisme de vigilancia, publicitat,
Cambridge Analytica i manipulació vot...

---

<section data-background-color="#F2F3EE">

### És molt complicat... No ho conec!

> Anem a conèixer les alternatives i a difondre-les!

<!-- .element: class="fragment fade-in" -->

---

<section data-background-color="#2A2A2A">

## Alternatives

---

<section data-background-color="#F2F3EE">

La XES i Colectic han recopilat eines lliures a l'abast de tothom!

<!-- .element: class="fragment fade-in-then-semi-out" -->

* **Repositori d'Eines Lliures, XES** (https://xes.cat/repositori-eines-lliures)
* **Recull d'Eines Lliures, Colectic** (https://floss.colectic.coop/eines)

<!-- .element: class="fragment fade-in" -->

---

<section data-background-color="#F2F3EE">

### Oficina 👩‍💻

| Per què?               | Eina                 | Alternativa de...          |
| :--------------------- | :------------------: |:--------------------------:|
| Eines ofimàtiques      | **LibreOffice**      | Microsoft Office           |
| Navegador web          | **Mozilla Firefox**  | Google Chrome, Safari      |
| Cercador               | **DuckDuckGo**       | Google                     |

<!-- .element: class="fragment fade-in" -->

Note: Anirem ara repassant diferents àmbits on és molt habitual en el dia a dia
utilitzar eines privatives. Comencem per les eines d'oficina que podem necessitar!
Fer pluja d'idees amb tothom sobre eines d'oficina, ofimàtica, etc.

---

<section data-background-color="#F2F3EE">

### Oficina 👩‍💻

Arran de la COVID, posem especial èmfasi en les eines lliures per **teletreballar**!

<!-- .element: class="text-dark-bg fragment fade-in" -->

https://floss.colectic.coop/eines-lliures-en-catal-per-teletreballar

<!-- .element: class="fragment fade-in" -->

---

<section data-background-color="#F2F3EE">

### Disseny gràfic 🎨

| Per què?               | Eina             | Alternativa de...          |
| :--------------------- | :--------------: |:--------------------------:|
| Retoc fotogràfic       | **GIMP**         | Photoshop                  |
| Maquetació editorial   | **Scribus**      | InDesign                   |
| Creació de prototips   | **Pencil**       | Balsamiq                   |

<!-- .element: class="fragment fade-in" -->

Note: Gimp (retoc fotogràfic), Scribus (maquetació editorial),
Pencil (creació prototips, maquetació web, mockups),
també Inkscape (disseny vectorial), Krita (il·lustració mapa de bits), etc.

---

<section data-background-color="#F2F3EE">

### Audiovisual 🎥🔉

| Per què?               | Eina             | Alternativa de...          |
| :--------------------- | :--------------: |:--------------------------:|
| Edició de so           | **Audacity**     | Adobe Audition, Oceanaudio |
| Modelat 3D, animació   | **Blender**      | After Effects              |
| Edició de vídeo        | **Flowblade**    | Windows Movie maker, iMovie |

<!-- .element: class="fragment fade-in" -->

---

<section data-background-color="#F2F3EE">

### Correu electrònic 📨

Com a alternativa a Gmail, Outlook, etc.
<!-- .element: class="fragment fade-in-then-semi-out" -->

Pangea, Disroot, Protonmail, Thunderbird

<!-- .element: class="text-dark-bg fragment fade-in" -->
Note:
Thunderbird és un gestor, no un compte per sé. El canviaria per Tuturota

---

<section data-background-color="#F2F3EE">

### Videoconferències 📞

Com a alternativa a Google Meet, MS Teams, Skype, etc.
<!-- .element: class="fragment fade-in-then-semi-out" -->

Jitsi, Framatalk

<!-- .element: class="text-dark-bg fragment fade-in" -->

---

<section data-background-color="#F2F3EE">

### Formularis i enquestes ✔️

Com a alternativa a Google Forms, Doodle, etc.
<!-- .element: class="fragment fade-in-then-semi-out" -->

Framaforms, LimeSurvey, dudle

<!-- .element: class="text-dark-bg fragment fade-in" -->

---

<section data-background-color="#F2F3EE">

### Eines per fer webs 🚧

Com a alternativa a Google Sites, etc.
<!-- .element: class="fragment fade-in-then-semi-out" -->

Drupal, Wordpress
<!-- .element: class="text-dark-bg fragment fade-in" -->

---

<section data-background-color="#F2F3EE">

### Documents col·laboratius 📋

Com a alternativa a Google Calendar, Sheets, etc.
<!-- .element: class="fragment fade-in-then-semi-out" -->

Framasoft (agenda, pad, calc, slides, date)
<!-- .element: class="text-dark-bg fragment fade-in" -->

Note:
Potser podriem no framatitzar-ho tot?
---

<section data-background-color="#F2F3EE">

### Treball al núvol ☁️

Com a alternativa a Google Drive, Dropbox, etc.
<!-- .element: class="fragment fade-in-then-semi-out" -->

NextCloud, Maadix, Pangea, Commonscloud, Framadrive
<!-- .element: class="text-dark-bg fragment fade-in" -->

Note:
Framadrive ja no està operatiu per nous comptes

---

<section data-background-color="#F2F3EE">

### Presa de decisions ✋🏽

Com a alternativa a Argu, Zenkit
<!-- .element: class="fragment fade-in-then-semi-out" -->

Decidim, Loomio, DemocracyOs
<!-- .element: class="text-dark-bg fragment fade-in" -->

---

<section data-background-color="#F2F3EE">

### I si ens hem deixat alguna eina?

[Cap problema!](https://floss.colectic.coop/eines)

<!-- .element: class="fragment fade-in" -->

Note: Buscar alguna eina que ens haguem deixat. Per exemple, ERP! També dir que
es pot buscar per eina privativa que volem substituir.
Molt recomanable també és alternativeto.net

---

<section data-background-color="#2A2A2A">

## Desavantatges

Note:
Aquest capítol és del 2 (Àngel)

---

<section data-background-color="#F2F3EE">

### Tràmits amb l'administració ☹️

Per fer tràmits amb segons quina administració necessito Windows/Internet Explorer/Word...

> Cada cop menys, i es pot resoldre amb partició o màquina virtual

<!-- .element: class="fragment fade-up" -->

Note:
Fa uns anys era bastant molest, però s'ha fet molta feina d'estàndards oberts
en aquest sentit

---

<section data-background-color="#F2F3EE">

### Intercanvi d'arxius 😖

M'han passat un docx que no s'obre bé amb el LibreOffice

> Cada cop menys, i podem ser part del problema o de la solució 😎

<!-- .element: class="fragment fade-up" -->

Note:
De nou s'ha fet molta feina d'estàndards oberts en aquest sentit

---

<section data-background-color="#F2F3EE">

### Pèrdua de capacitat de comunciació 📢

Si no estic a Whatsapp, no hi ha tothom; si no estic a Instagram, no em veuen...

> Podem fer servir les eines del capital per lluitar contra ell, però fem-ho
> de forma conscient

<!-- .element: class="fragment fade-up" -->

---

<section data-background-color="#F2F3EE">

### Per fer X no hi ha alternativa viable 😔

A vegades, ens hi podem trobar, cercar i que no hi hagi res...

> Podem fer tota la resta amb FLOSS.  No es tracta de blanc o negre.
<!-- .element: class="fragment fade-up" -->

Note:
Com en el transport, a vegades hem d'agafar el cotxe
---

<section data-background-color="#F2F3EE">

## Recuperem les paraules!

##### _Ho hem encertat tot?_
<!-- .element: class="fragment fade-up" -->

##### _Trobeu a faltar algun concepte?_
<!-- .element: class="fragment fade-up" -->

---

<section data-background-color="#F2F3EE">

### Esperem que ara ja us sentiu així!

![memefloss](img/memefloss.jpg "Meme FLOSS (Gràcies Miquel!)")

<!-- .element: class="fragment fade-up" -->

Si voleu aquesta presentació, cliqueu [aquí](https://anxodio.gitlab.io/floss-pamapam/#/)!
O el codi, consulteu el [repositori](https://gitlab.com/anxodio/floss-pamapam/).
<!-- .element: class="fragment fade-up" -->

---

<section data-background-color="#2A2A2A">

# Moltes gràcies!

---
