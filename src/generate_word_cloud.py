"""

"""

import fire
import matplotlib.pyplot as plt
from pathlib import Path
import re
from wordcloud import WordCloud, STOPWORDS


def plot_cloud(wordcloud, filename):

    plt.figure(figsize=(40, 30))
    plt.imshow(wordcloud)
    plt.axis("off");
    plt.savefig(Path('img') / f'{filename}.png')
    plt.show()


def generate_and_plot_word_cloud(filepath):

    with open(filepath, 'r') as f:
        text = f.read()

    wordcloud = WordCloud(width=3000, height=2000, random_state=1, background_color='#F49339', colormap='Pastel1',
                          collocations=False, stopwords=STOPWORDS).generate(text)
    # Plot
    plot_cloud(wordcloud, Path(filepath).stem)


if __name__ == '__main__':
    # python src/generate_word_cloud.py --filepath='input/test.txt'
    # python src/generate_word_cloud.py --filepath='input/initial_words.txt'
    # python src/generate_word_cloud.py --filepath='input/final_words.txt'
    fire.Fire(generate_and_plot_word_cloud)
