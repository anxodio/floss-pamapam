# Formació FLOSS Pam a Pam

Com executar la presentació en local:

```
reveal-md slides.md -w
```

Docu a: https://github.com/webpro/reveal-md

Exemple presentació a: https://gitlab.com/anxodio/socialissues-slides-sobtec
